#!/usr/bin/env bash

# make sure we have pulled in and updated any submodules
git submodule init
git submodule update

# system config installation (root permission needed)
system=(
)

# what directories should be installable by all users including the root user
base=(

)

# folders that should, or only need to be installed for a local user
useronly=(
    base
    bin
    chromium
    flashfocus
    firefox
    git
    gnupg
    gsimplecal
    htop
    kakoune
    kitty
    mail
    mako
    mattermost
    mpv
    neovim
    nnn
    pacman
    qalculate
    qutebrowser
    redshift
    repoctl
    swappy
    sway
    swaylock
    systemd
    tig
    transmission
    urlwatch
    usbguard
    vimiv
    waybar
    wofi
    zsh
)

# run the stow command for the passed in directory ($2) in location $1
stowit() {
    usr=$1
    app=$2
    # -v verbose
    # -R recursive
    # -t target
    stow -v -R -t ${usr} ${app}
}

echo ""
echo "Stowing apps for user: ${whoami}"

# install system config files if root
for app in ${system[@]}; do
    if [[ "$(whoami)" = *"root"* ]]; then
        stowit "/" $app
    fi
done

# install apps available to local users and root
for app in ${base[@]}; do
    stowit "${HOME}" $app
done

# install only user space folders
for app in ${useronly[@]}; do
    if [[ ! "$(whoami)" = *"root"* ]]; then
        stowit "${HOME}" $app
    fi
done

echo ""
echo "##### ALL DONE"
